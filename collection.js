function Collection () {}

Collection.prototype = new Array();

Collection.prototype.sum = function () {
	var x = 0;

	this.forEach(function(elem) {
		x+=elem;
	});

	return x;
}


var c = new Collection();
var z = new Collection();