var contactForm = document.forms['calculadora'];

contactForm.onsubmit = function() {
	return false;
};

var somar = contactForm.getElementsByClassName('bt-somar')[0];

somar.onclick = function() {
	var parcelas = contactForm.getElementsByClassName('calc');

	var x = 0;

	parcelas = Array.prototype.slice.call(parcelas);

	parcelas.forEach(function(elem) {
		x += parseInt( elem.value );
	});

	contactForm.result.value = x;
}